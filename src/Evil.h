#ifndef Evil_H

// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

// Common Structures
#ifdef __clang__
#define Main int main(int argc, char** argv){
#else
#define Main stdint main(stdint argc, char** argv){
#endif

#define End }
#define If if(
#define While while(
#define For for(
#define Begin ){
#define Then ){
#define Else } else {
#define Elif } else if(

// Function Helpers
#define Fn(t_, var_) t_ var_(
#define uses
#define and ;

// Types
#define Int(v_) stdint v_
#define Double(v_) double v_
#define Float(v_) float v_
#define Char(v_) char v_
#define Void(v_) void v_
#define Bool(v_) bool v_
#define Struct struct
#define Union union

#define intPtr(v_) stdint *v_
#define doublePtr(v_) double *v_
#define floatPtr(v_) float *v_
#define charPtr(v_) char *v_
#define voidPtr(v_) void *v_
#define boolPtr(v_) bool *v_
#define structPtr(s_, v_) struct s_ *v_
#define unionPtr(u_, v_) union u_ *v_

#define intDbPtr(v_) stdint **v_
#define doubleDbPtr(v_) double **v_
#define floatDbPtr(v_) float **v_
#define charDbPtr(v_) char **v_
#define voidDbPtr(v_) void **v_
#define boolDb(v_) bool **v_
#define structDbPtr(s_, v_) struct s_ **v_
#define unionDbPtr(u_, v_) union u_ **v_

#define Type(t_, v_) t_ v_

// Inbuilts

#define return(i_) return i_

#define foreach(i_, a_) for(#i_ = 0; #i_ < (sizeof a_ / sizeof *a_); i_++

// print and println definitions by Robert Gamble: http://www.robertgamble.net/2012/01/c11-generic-selections.html
// use (0, x) to forrce decay to pointer
#define printf_dec_format(x_) _Generic((0, x_), \
    char: "%c", \
    signed char: "%hhd", \
    unsigned char: "%hhu", \
    signed short: "%hd", \
    unsigned short: "%hu", \
    signed int: "%d", \
    unsigned int: "%u", \
    long int: "%ld", \
    unsigned long int: "%lu", \
    long long int: "%lld", \
    unsigned long long int: "%llu", \
    float: "%f", \
    double: "%f", \
    long double: "%Lf", \
    char *: "%s", \
    void *: "%p")

#define print(x_) printf(printf_dec_format(x_), x_)
#define println(x_) printf(printf_dec_format(x_), x_); printf("\n")

#define Switch(v_) switch(v_) {
#define Case(i_) case i_: {
#define Default default: {

#define pass do {} while (0)

#define comment(...); /* ##__VA_ARGS__ */

// Set stdint
#if UINTPTR_MAX == 0xffffffff
#define stdint int32_t
#elif UINTPTR_MAX == 0xffffffffffffffff
#define stdint int64_t
#else
#define stdint int16_t
#endif

#endif
